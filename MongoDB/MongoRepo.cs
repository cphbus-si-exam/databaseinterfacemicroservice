using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using databaseInterface.Models;
using databaseInterfaceAPI.MongoDB;
using MongoDB.Bson;
using MongoDB.Driver;

namespace databaseInterface.MongoDB
{
    public class MongoRepo : IMongoRepo
    {
        string connectionString, collectionName, DBName;
        MongoClient client;
        IMongoDatabase mongoDatabase;
        public MongoRepo()
        {
            connectionString = Environment.GetEnvironmentVariable("MONGODB_CS");
            DBName = "eventDB";
            client = new MongoClient(connectionString);
            mongoDatabase = client.GetDatabase(DBName);
            collectionName = "events";
        }

        //Create
        public async Task<bool> CreateNewEventAsync(EventModel eventData)
        {
            try
            {
                var collection = mongoDatabase.GetCollection<EventModel>(collectionName);
                eventData.state = "pending";
                await collection.InsertOneAsync(eventData);
                return true;
            }
            catch (System.Exception)
            {
                return false;
            }
        }
        //update eventstate
        public async Task<bool> setEventStateAsync(string eID, string newState)
        {
            try
            {
                var collection = mongoDatabase.GetCollection<EventModel>(collectionName);
                var filter = Builders<EventModel>.Filter.Eq("_id", ObjectId.Parse(eID));
                var EventData = (await collection.FindAsync(x => x.eventid == ObjectId.Parse(eID))).FirstOrDefault();
                EventData.state = newState;
                collection.ReplaceOne(filter, EventData);
                return true;
            }
            catch (System.Exception)
            {
                return false;
            }
        }

        public async Task<bool> UpdateEventAsync(EventModel eventData)
        {

            try
            {
                var collection = mongoDatabase.GetCollection<EventModel>(collectionName);
                var filter = Builders<EventModel>.Filter.Eq("_id", eventData.eventid);
                await collection.ReplaceOneAsync(filter, eventData);
                return true;
            }
            catch (System.Exception)
            {
                return false;
            }
        }

        public async Task<bool> DeleteEventAsync(string eID)
        {
            try
            {
                await setEventStateAsync(eID, "Deleted");
                return true;
            }
            catch (System.Exception)
            {
                return false;
            }
        }
        //read specific
        public async Task<EventModel> GetEventAsync(string eID)
        {
            try
            {
                var collection = mongoDatabase.GetCollection<EventModel>(collectionName);
                var res = (await collection.FindAsync(x => x.eventid == ObjectId.Parse(eID))).FirstOrDefault();
                return res;
            }
            catch (System.Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        // read all
        public async Task<List<EventModel>> GetAllEventsAsync()
        {
            try
            {
                var colllection = mongoDatabase.GetCollection<EventModel>(collectionName);
                var events = (await colllection.FindAsync(x => true)).ToList();
                return events;
            }
            catch (System.Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}