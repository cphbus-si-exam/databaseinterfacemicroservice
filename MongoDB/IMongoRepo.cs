using System.Collections.Generic;
using System.Threading.Tasks;
using databaseInterface.Models;

namespace databaseInterfaceAPI.MongoDB
{
    public interface IMongoRepo
    {
        Task<bool> CreateNewEventAsync(EventModel eventData);
        Task<bool> DeleteEventAsync(string eID);
        Task<List<EventModel>> GetAllEventsAsync();
        Task<EventModel> GetEventAsync(string eID);
        Task<bool> setEventStateAsync(string eID, string newState);
        Task<bool> UpdateEventAsync(EventModel eventData);
    }
}