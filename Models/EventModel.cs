using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace databaseInterface.Models
{
    public class EventModel
    {
        //public ObjectId _id { get; set; }
        [BsonId]
        public ObjectId eventid { get; set; }
        public string eventtype { get; set; }
        public int participantkey { get; set; }
        public string state { get; set; }
        public VenueModel venue { get; set; }
        public AccommodationModel accommodation { get; set; }
        public CateringModel catering { get; set; }
        public List<TransportModel> transport { get; set; }
        public string request_type { get; set; }

    }
}