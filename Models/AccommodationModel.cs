namespace databaseInterface.Models
{
    public class AccommodationModel
    {
        public int numberofrooms { get; set; }
        public string startdate { get; set; }
        public string enddate { get; set; }
        public string request_type { get; set; }
    }
}