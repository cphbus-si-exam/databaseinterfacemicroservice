namespace databaseInterface.Models
{
    public class TransportModel
    {
        public int numberofpersons { get; set; }
        public string pickupdate { get; set; }
        public string pickuplocation { get; set; }
        public string dropofflocation { get; set; }
        public string type { get; set; }
        public string request_type { get; set; }

    }
}