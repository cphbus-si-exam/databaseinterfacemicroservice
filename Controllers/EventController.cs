using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using databaseInterface.Models;
using databaseInterfaceAPI.Models;
using databaseInterfaceAPI.MongoDB;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;

namespace databaseInterfaceAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EventController : Controller
    {
        private readonly IMongoRepo ctx;
        public EventController(IMongoRepo ctx)
        {
            this.ctx = ctx;
        }


        [HttpGet("{id}")]
        public async Task<JsonResult> Get(string id){
            if (id == "alive"){
                return Json("DatabaseAPI is alive");
            }
            try{
                return Json(await ctx.GetEventAsync(id));
            }
            catch (System.Exception ex){
                throw new Exception(ex.Message);
            }
        }


        [HttpGet]
        public async Task<JsonResult> GetAll(){
            try{
                return Json(await ctx.GetAllEventsAsync());
            }
            catch (System.Exception ex){
                throw new Exception(ex.Message);
            }
        }
        
        [HttpPost]
        public async Task<JsonResult> Post([FromBody]EventModel edata){
            try{
                var res = await ctx.CreateNewEventAsync(edata);
                var resmodel = new EventStatusModel(){status=res, eventid=edata.eventid.ToString()};
                return Json(resmodel);
            }
            catch (System.Exception){
                return Json(HttpStatusCode.BadRequest);
            }
        }

        [HttpPatch("{id}/status/{state}")]
        public async Task<JsonResult> Patch(string id, string state){
            try
            {
                var res = new EventStatusModel();
                await ctx.setEventStateAsync(id, state);
                res.eventid = id;

                if (state == "confirmed"){
                    res.status = true;
                }else{
                    res.status = false;
                }
                return Json(res);
            }
            catch (System.Exception)
            {
                return Json(HttpStatusCode.BadRequest);
            }
        }

        [HttpPatch("{id}/venue")]
        public async Task<JsonResult> PatchVenue([FromBody]VenueModel venueData, string id){
            try
            {
                var oldData = await ctx.GetEventAsync(id);
                var newVenue = new VenueModel(){numberofpersons=venueData.numberofpersons, startdate=venueData.startdate, enddate=venueData.enddate, address=venueData.address};
                var newData = new EventModel(){eventid = oldData.eventid, eventtype=oldData.eventtype, participantkey=oldData.participantkey, state=oldData.state, venue=newVenue, transport=oldData.transport, accommodation=oldData.accommodation, catering=oldData.catering};
                var res = await ctx.UpdateEventAsync(newData);
                return Json(res);
            }
            catch (System.Exception)
            {
                return Json(HttpStatusCode.BadRequest);
            }
        }


        [HttpPatch("{id}/accommodation")]
        public async Task<JsonResult> PatchAcc([FromBody]AccommodationModel accData, string id){
            try
            {
                var oldData = await ctx.GetEventAsync(id);
                var newAcc = new AccommodationModel(){numberofrooms=accData.numberofrooms, startdate=accData.startdate, enddate=accData.enddate};
                var newData = new EventModel(){eventid = oldData.eventid, eventtype=oldData.eventtype, participantkey=oldData.participantkey, state=oldData.state, venue=oldData.venue, transport=oldData.transport, accommodation=newAcc, catering=oldData.catering};
                var res = await ctx.UpdateEventAsync(newData);
                return Json(res);
            }
            catch (System.Exception)
            {
                return Json(HttpStatusCode.BadRequest);
            }
        }

        [HttpPatch("{id}/transport")]
        public async Task<JsonResult> PatchTra([FromBody]List<TransportModel> transporData, string id){
            try
            {
                var translist = new List<TransportModel>();
                foreach (var item in transporData){
                    var newTra = new TransportModel(){numberofpersons=item.numberofpersons, pickupdate=item.pickupdate, pickuplocation=item.pickuplocation, dropofflocation= item.dropofflocation, type= item.type};
                    translist.Add(newTra);
                }

                var oldData = await ctx.GetEventAsync(id);
                var newData = new EventModel(){eventid = oldData.eventid, eventtype=oldData.eventtype, participantkey=oldData.participantkey, state=oldData.state, venue=oldData.venue, transport=translist, accommodation=oldData.accommodation, catering=oldData.catering};
                var res = await ctx.UpdateEventAsync(newData);
                return Json(res);
            }
            catch (System.Exception)
            {
                return Json(HttpStatusCode.BadRequest);
            }
        }

        [HttpPatch("{id}/catering")]
        public async Task<JsonResult> PatchCat([FromBody]CateringModel catData, string id){
            try
            {
                var meallist = new List<string>();
                foreach (var item in catData.type){
                    meallist.Add(item);
                }

                var oldData = await ctx.GetEventAsync(id);
                var newCat = new CateringModel(){normalmeal=catData.normalmeal, specialmeal=catData.specialmeal, startdate=catData.startdate, enddate=catData.enddate, type=meallist};
                var newData = new EventModel(){eventid = oldData.eventid, eventtype=oldData.eventtype, participantkey=oldData.participantkey, state=oldData.state, venue=oldData.venue, transport=oldData.transport, accommodation=oldData.accommodation, catering=newCat};
                var res = await ctx.UpdateEventAsync(newData);
                return Json(res);
            }
            catch (System.Exception)
            {
                return Json(HttpStatusCode.BadRequest);
            }
        }


        [HttpDelete("{id}")]
        public async Task<JsonResult> Delete(string id){
            try{
                var res = await ctx.DeleteEventAsync(id);
                return Json(res);
            }
            catch (System.Exception){
                return Json(HttpStatusCode.BadRequest);                
            }    
        }
    }
}